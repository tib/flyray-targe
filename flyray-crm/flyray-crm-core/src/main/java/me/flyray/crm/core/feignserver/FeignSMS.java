package me.flyray.crm.core.feignserver;

import me.flyray.crm.core.biz.SMSBiz;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.flyray.crm.facade.request.SendSMSRequest;
import me.flyray.crm.facade.request.ValidateSMSRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.Map;

/***
 * 短信相关服务
 * */
@Api(tags="短信相关服务")
@Controller
@RequestMapping("feign/sms")
public class FeignSMS {
	
	@Autowired
	private SMSBiz smsBiz;
	
	/**
	 * 发送短信并保存到Redis中
	 * @author centerroot
	 * @time 创建时间:2018年11月19日下午5:12:31
	 * @param sendSMSRequest
	 * @return
	 */
	@ApiOperation("发送短信并保存到Redis中")
	@RequestMapping(value = "/sendCode",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> sendCode(@RequestBody @Valid SendSMSRequest sendSMSRequest){
		Map<String, Object> respMap = smsBiz.sendSmsCode(sendSMSRequest);
		return respMap;
    }
	
	
	@ApiOperation("校验短信验证码")
	@RequestMapping(value = "/validateSmsCode",method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> validateSmsCode(@RequestBody @Valid ValidateSMSRequest validateSMSRequest){
		Map<String, Object> respMap = smsBiz.validateSmsCode(validateSMSRequest);
		return respMap;
    }
	
}
