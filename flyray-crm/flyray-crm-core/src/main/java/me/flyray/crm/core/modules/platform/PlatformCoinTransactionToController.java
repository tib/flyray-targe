package me.flyray.crm.core.modules.platform;

import me.flyray.crm.core.biz.platform.PlatformCoinTransactionToBiz;
import me.flyray.crm.core.entity.PlatformCoinTransactionTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import me.flyray.common.rest.BaseController;

@Controller
@RequestMapping("platformCoinTransactionTo")
public class PlatformCoinTransactionToController extends BaseController<PlatformCoinTransactionToBiz, PlatformCoinTransactionTo> {

}