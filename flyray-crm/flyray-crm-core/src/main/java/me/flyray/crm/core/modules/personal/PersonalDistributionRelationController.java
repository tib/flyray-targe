package me.flyray.crm.core.modules.personal;

import me.flyray.crm.core.entity.PersonalDistributionRelation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.common.rest.BaseController;
import me.flyray.crm.core.biz.personal.PersonalDistributionRelationBiz;

@RestController
@RequestMapping("personalDistributionRelation")
public class PersonalDistributionRelationController extends BaseController<PersonalDistributionRelationBiz, PersonalDistributionRelation> {

}